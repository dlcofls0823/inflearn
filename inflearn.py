# -*- coding: utf-8 -*-
import json
import re
import requests
import urllib.request
import urllib.parse

from bs4 import BeautifulSoup
from flask import Flask, request
from slack import WebClient
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.interactions import MessageInteractiveEvent
from slackeventsapi import SlackEventAdapter
from urllib import parse


SLACK_TOKEN = "xoxb-682997044292-683285284977-xMhg77roUdgx3AlMbDhd2lcK"
SLACK_SIGNING_SECRET = "f30a2fc15da4c6e219cfb6da9b16716f"


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)



# 크롤링 함수 구현하기
def _crawl(text):
    if "help" in text : 
        help_section = SectionBlock(
            text = ":four_leaf_clover:"+ "인프런 강의를 카테고리, 스킬, 난이도, 가격별로 알려드리는 챗봇입니다! :-)\n"+ ":black_small_square:"+ "카테고리 : it프로그래밍, 웹개발, 모바일앱, 게임개발, 데이터사이언스, 보안, 블록체인, 알고리즘, 교양, 수학, 데이터베이스, 개발도구, 프레임워크및라이브러리, 프로그래밍언어, 서비스개발,  인프라, 사물인터넷\n" + ":black_small_square:"+"스킬 : Javascript, Java, Android, html/css, Nodejs, iOS, firebase, Front-end, React, Spring 등 영어로 작성\n" + ":black_small_square:"+"난이도 : 입문, 초급, 중급, 활용, 입문초급, 초급중급, 중급활용\n" + ":black_small_square:"+"가격 : 유료, 무료\n" + ":black_small_square:"+ "*입력 예시 - ' it-programming html/css 입문초급 무료 '*\n"
        )
        return [help_section]


    text = text.split()
    print(text)
    if len(text)>3 : 
        level_text = text[3]
        if "입문" in level_text:
            if "초급" in level_text:
                level_text = "level-1,level-2"
            else:
                level_text = "level-1"
        elif "초급" in level_text:
            if "중급" in level_text:
                level_text = "level-2,level-3"
            else:
                level_text = "level-2"

        elif "중급" in level_text:
            if "활용" in level_text:
                level_text = "level-3,level-4"
            else:
                level_text = "level-3"
        elif "활용" in level_text:
            level_text = "level-4"

    if len(text)>4 : 
        charge_text = text[4]
        if "유료" in charge_text:
            charge_text = "paid"
        elif "무료" in charge_text:
            charge_text = "free"
 
    url = "https://www.inflearn.com/courses/"
    # print(text)
    # print(url)
    sourcecode = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(sourcecode, "html.parser")

    csdic = {} #코스명 ->주소 변환 딕셔너리
    #itprogramming , itprogramming/web-dev
    allnum = 1 # all갯수 에따라 전체메뉴 설정 
    for links in soup.find_all("div", class_="accordion-body") :
        for link in links.find_all("a") :
            key = link.get_text().strip()
            if key == 'ALL' :
                if allnum == 1:
                    key ='it프로그래밍'
                    allnum +=1 
                elif allnum == 2:
                    key ='디자인'
                    allnum +=1
                elif allnum == 3:
                    key ='비즈니스'
                    allnum +=1
                elif allnum ==4:
                    key ='마케팅'
                    allnum +=1 
            if 'href' in link.attrs:
                csdic[key] = link.attrs['href']
    # print(csdic)
    for i in csdic.keys() :
        if text[1] in i :
            url = "https://www.inflearn.com"+csdic[i]
            # print(url)
            break
    skstring = []
    sk_key = [] 
    skdic = {}

    ssourcecode = urllib.request.urlopen(url).read()
    soupp = BeautifulSoup(ssourcecode, "html.parser")

    if len(text)>2 :   
        for sdata in soupp.find_all("button", class_="tag skill button"):
            skstring = str(sdata).split("\"")
            skdic[sdata.get_text().strip()] = skstring[9]
        # print(skdic)
        for skn in skdic.keys() :
            if text[2] in skn :
                url = url + "?skill=" + skdic[skn]
                # print(url+"스킬더해용")
                break


    ####################난이도
    if len(text)>3 :
        url = url +  "&level=" + level_text
    
    # print(url)
    ####################유료/무료
    if len(text)>4 :
        url = url +  "&charge=" + charge_text
    # print(url)
    
    ssourcecode = urllib.request.urlopen(url).read()
    soupp = BeautifulSoup(ssourcecode, "html.parser")
    # url = "https://www.inflearn.com/courses/" + category_text +"?skill=" + skill_text + "&level=" + level_text + "&order=seq"


    ctitle = []
    teacher = []
    price = [] 
    img = []
    link = [] 

       ############데이터 가져오기  ############# 
    for dats in soupp.find_all("div",class_="card-content") : 
        for cdata in dats.find_all("div",class_="course_title"):
            ctitle.append("*"+cdata.get_text().strip()+"*")

    for tdata in soupp.find_all("div",class_="instructor has-text-right column is-half"):
        teacher.append(tdata.get_text())
    for pdata in soupp.find_all("div" ,class_="price has-text-right column is-half") :
        price.append(pdata.get_text().strip())        
    for lk in soupp.find_all("div", class_="card course") :
        img.append(lk.find("div", class_="card-image").find("figure",class_="image is_thumbnail").find("img")["src"])
        links = lk.find("a")["href"]
        link.append("https://www.inflearn.com"+parse.quote(links))    
   
    items = [] # 한 코스정보가 딕셔너리로 구성된 리스트 
    attachment_list = [] 
    message_list = [] 
    attachments = [] 
    if len(ctitle) > 6 :
        n = 6
    elif len(ctitle)> 0 : 
        n = len(ctitle)
    else : 
        n = 0
        message_list.append(' ')
        attachments = [{"text": ":negative_squared_cross_mark:" + "검색 결과가 없습니다.:-(\n \"help\" 를 입력하여 사용법을 익혀보세요!\n"}]
        attachment_list.append(attachments)

    if n > 0 :
        for num in range(n) : 
            items.append({
                "course" : ctitle[num],
                "teacher" : teacher[num],
                "price" : price[num],
                "image" : img[num],
                "link"  : link[num]
                })
            txt = "<" + link[num]+ "|"+ ctitle[num] +">" +"\n"+  teacher[num] + "\n"+price[num] + "\n\n\n\n" 
            attachments = [{"text": txt,"type":"image", "thumb_url":img[num] }]
            message_list.append('')
            attachment_list.append(attachments)
        attachments = [{
            "text": "정렬 순서를 선택하세요 :) ",
            "fallback": "choose how to sorted",
            "callback_id": "game",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "actions": [
                {
                    "name": "game",
                    "text": "추천순",
                    "type": "button",
                    "value": "text[1:]+" ,
                    "confirm": {
                        "title": "변경하시겠습니까?",
                        "text": "강의를 추천순으로 보길 원하시나요? ",
                        "ok_text": "Yes",
                        "dismiss_text": "No"
                    }
                },
                {
                    "name": "game",
                    "text": "인기순",
                    "type": "button",
                    "value": "popular",
                    "confirm": {
                        "title": "변경하시겠습니까?",
                        "text": "강의를 추천순으로 보길 원하시나요? ",
                        "ok_text": "Yes",
                        "dismiss_text": "No"
                    }
                },
                {
                    "name": "game",
                    "text": "평점순",
                    "style": "danger",
                    "type": "button",
                    "value": "ract",
                    "confirm": {
                        "title": "변경하시겠습니까?",
                        "text": "강의를 추천순으로 보길 원하시나요? ",
                        "ok_text": "Yes",
                        "dismiss_text": "No"
                    }
                }
            ]
        }
        ]
        attachment_list.append(attachments)
        message_list.append('')
    return message_list,attachment_list

    print(url)
    txt = "\n"
   
    

    # if len(items)>5 :
    #     txt = "\n"
    #     for item in items[:5] :
    #         txt += "<" + link[num]+ "|"+ item["course"] +">" +"\n"+ item["teacher"]+ "\n"+item["price"] + "\n\n\n\n" 
    #     link_section = SectionBlock( text = txt, accessory= get_image )
    # elif len(items)>0:
    #     for item in items[0:] :
    #         txt += "<" + link[num]+ "|"+ item["course"] +">" +"\n"+ item["teacher"]+ "\n"+item["price"] + "\n\n\n\n" 
    #     link_section = SectionBlock( text = txt,accessory= get_image)
    # else :
    #     txtt = "'help'"
    #     link_section = SectionBlock(text = "검색 결과가 없습니다.:-(\n" +txtt +"를 입력하여 사용법을 익혀보세요!")
            
    # return [link_section]


prev_client_msg_id = {}
# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    client_msg_id =event_data["event"]["client_msg_id"]
    if client_msg_id not in prev_client_msg_id :
        prev_client_msg_id[client_msg_id] = 1
    else:
        return

    if 'help' in text :      
        message_blocks = _crawl(text)
        slack_web_client.chat_postMessage(
            channel=channel,
            # text=message,
            blocks = extract_json(message_blocks)
        )
    else :
        message_list,attachments_list = _crawl(text)
        # if message_list[0] is "No" : 
        #     slack_web_client.chat_postMessage(
        #         channel=channel,
        #         # text = message_list[0],
        #         attachments = attachments_list
        #     )
     
        for i in range(len(message_list)):
            slack_web_client.chat_postMessage(
                channel=channel,
                text = message_list[i],
                attachments = attachments_list[i]
            )

@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    # 버튼 클릭은 SlackEventsApi에서 처리해주지 않으므로 직접 처리합니다
    payload = request.values["payload"]
    click_event = MessageInteractiveEvent(json.loads(payload))

    new_order = click_event.value

    # 다른 가격대로 다시 크롤링합니다.
    message_blocks = _crawl(new_order)

    # 메시지를 채널에 올립니다
    slack_web_client.chat_postMessage(
        channel=click_event.channel.id,
        blocks=extract_json(message_blocks)
    )

    # Slack에게 클릭 이벤트를 확인했다고 알려줍니다
    return "OK", 200

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)